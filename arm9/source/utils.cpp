#include "utils.h"



PrintConsole topScreen;
PrintConsole bottomScreen;


void printTop(const char* str, ...){
	REG_IME	= 0;
	va_list args;
	va_start(args, str);
	consoleSelect(&topScreen);
	vprintf(str, args);
	consoleSelect(&bottomScreen);
	va_end(args);
	REG_IME = 1;
}




void initGraphics(){
	videoSetMode(MODE_0_2D);
	videoSetModeSub(MODE_0_2D);
	vramSetBankA(VRAM_A_MAIN_BG);
	vramSetBankC(VRAM_C_SUB_BG);
	consoleInit(&topScreen, 3,BgType_Text4bpp, BgSize_T_256x256, 31, 0, true, true);
	consoleInit(&bottomScreen, 3,BgType_Text4bpp, BgSize_T_256x256, 31, 0, false, true);
	consoleSelect(&bottomScreen);
}





std::string getFile(const char* t, std::string &dir){
	Menu *m = new Menu;
	m->setTitle(t);
	dirent *e;
	DIR *dp = opendir(dir.c_str());
	if(!dp){
		printTop("Unable to open directory.\n");
		closedir(dp);
		return "";
	}
	while((e = readdir(dp))){
		std::string s(e->d_name);
		if(s.length() > 26){s = s.substr(0, 26);}
		if(e->d_type == DT_DIR){s+="\\";}
		m->addOption(s);
	}
	int choice = m->getDecision();
	delete m;
	
	//B was pressed
	if(choice == -1){
		if(dir.length() <= 2){ //at root
			closedir(dp);
			return "";
		}
		dir.erase(dir.find_last_of("/"));
		closedir(dp);
		return getFile(t, dir);
	}
	seekdir(dp, choice);
	e = readdir(dp);
	
	//a directory is selected
	if(e->d_type == DT_DIR){ 
		std::string chosenDir = e->d_name;
		if(chosenDir == ".."){
			dir.erase(dir.find_last_of("/"));
			closedir(dp);
			return getFile(t, dir);
		}
		if(chosenDir == "."){
			closedir(dp);
			return getFile(t, dir);
		}
		dir+="/";
		dir+=e->d_name;
		closedir(dp);
		return getFile(t, dir);
	}
	
	//a file is selected
	if(e->d_type == DT_REG){
		dir+="/";
		dir+=e->d_name;
		closedir(dp);
		return dir;
	}
	closedir(dp);
	return "";
}




int askMulticartOffset(){
	u16 offset = 0;
	do{
		scanKeys();
		if(keysDown() & KEY_LEFT){
			if(offset >= 4)
			offset-=4;
		}
		if(keysDown() & KEY_RIGHT){
			offset+=4;
		}
		
		if(keysDown() & KEY_DOWN){
			if(offset >= 32)
			offset-=32;
		}
		if(keysDown() & KEY_UP){
			offset+=32;
		}
		
		if(offset > 252){offset = 252;}
		
		swiWaitForVBlank();
		consoleClear();
		printf("\n\n\tChoose ROM starting offset:\n\n");
		printf("\tOffset: %d MB\n\t\t\t  0x%08X\n\n", offset, offset*1024*1024);
		printf("\tD-Pad: Select\n\tA: Confirm\t B: Cancel");
	}while((keysDown() & (KEY_A | KEY_B)) == 0);
	
	if(keysDown() & KEY_A){
		return offset;
	}
	return -1;
}



bool isGame(){
	unsigned char* nintendo_logo = (unsigned char*)0x8000004;
	unsigned long checksum = 0;
	for(unsigned int i = 0;i<0x9C;i++){
		checksum += nintendo_logo[i];
	}
	return checksum == 0x4B1B;
}


void listMulticartGames(){
	// backup start of sram which gets overwritten by EG0xxGotoChipOffset
	u8 sramBuffer[3]; 
	sramBuffer[0] = *(vu8*)0x0A000002;
	sramBuffer[1] = *(vu8*)0x0A000003;
	sramBuffer[2] = *(vu8*)0x0A000004;
	const auto restoreSram = [sramBuffer](){
		*(vu8*)0x0A000002 = sramBuffer[0];
		*(vu8*)0x0A000003 = sramBuffer[1];
		*(vu8*)0x0A000004 = sramBuffer[2];
	};
	
	int games = 0;
	char names[64*13];
	u8 addresses[64];
	
	for(u16 offset = 0; offset < 256; offset+=4){
			EG0xxGotoChipOffset(offset);
			if(isGame()){
				strncpy(names+13*games, (char*)0x80000A0, 12);
				names[13*games+12] = 0;
				addresses[games] = offset;
				games++;
			}
	}
	
	if(!games){
		printTop("No games found!\n");
		restoreSram();
		return;
	}
	
	if(games == 64){
	    int cmp = 0;
		for(int i = 1;i<64;i++){
			cmp += strcmp(names+13*i, names);
		}
		if(!cmp){
			printTop("No bank switching detected\n");
			restoreSram();
			return;
		}
	}
	
	Menu list("Offset(MB)\tGame title ()");
	
	for(int i = 0;i<games;i++){
		char offsetStr[4];
		itoa(addresses[i], offsetStr, 10);
		std::string gameInfo(offsetStr);
		gameInfo += "\t";
		gameInfo += (char*)(names+i*13);
		list.addOption(gameInfo);
	}
	
	list.addOption("Back");
	
	int x;
	do{
		x = list.getDecision();
		if(x != -1 && x != games){
			EG0xxGotoChipOffset(addresses[x]);
			printTop("Switched bank to %d!\n", addresses[x]);
			return;
		}
	}while(x != -1 && x != games);
}



void detectionTest(){
    const char* carts[6] = {"Unknown", "MSP100", "MSP128", "Intel", "Intel buffered", "MSP100 (address shifted)"};
	const char* yesno[2] = {"No", "Yes"};
	while(1){
			cartType result = detect();

			consoleClear();
			printf("Detection test: \n\nCart type: %s\n", carts[(int)result.method]);
			if(getFlipDataBitsFlag(result)){
				printf("\t(flipped data bits 0 & 1)\n");
			}
		
		if(isGame()){
			printf("\nHeader mirrors(MB): ");
			int romsize = 32;
			for(int mb = 4;mb <= 16; mb *= 2){
				if(isHeaderMirroredAt(mb * 1024 * 1024)){
					if(romsize > mb) romsize = mb;
					printf("%d ", mb);
				}else{
					printf("-- ");
				}
			}
			printf("\n\t=> Flash size: %d MB\n", romsize);

		}

		int hasSramBanks = checkSramBankSwitching();
		printf("\nSRAM Bank Switching: %s\n", yesno[hasSramBanks]);

		printf("\n\n\nPress B to go back.\n");
		
		for(int i = 0;i<15;i++){ //wait 15 frames between detects
	    		scanKeys();
			if(keysDown() & KEY_B){
			    return;
			}
			swiWaitForVBlank();
		}
	}
}






void updateUI(){
	gStatus.seconds++;
	consoleClear();
	printf("\x1b[2;6H=== Flashing ROM ===\x1b[13;4HTime elapsed: %d seconds\n\x1b[7;1HErasing sectors(%d / %d)", gStatus.seconds, gStatus.sectorsErased, gStatus.totalSectors);
	if(gStatus.sectorsErased == gStatus.totalSectors){
		printf("\x1b[9;1HWriting sectors(%d / %d)", (int)gStatus.wordsFlashed/0x10000, (int)gStatus.totalSectors);
	}
}


void startFlashingUI(){
	gStatus.seconds = 0;
	gStatus.sectorsErased = 0;
	gStatus.wordsFlashed = 0;
	timerStart(0, ClockDivider_1024, TIMER_FREQ_1024(1), updateUI);
}

void stopFlashingUI(){
	timerStop(0);
}





int verifyFile(FILE* fp, u32 refDataAddress){
	if(!fp){
		printTop("No file to verify\n");
		return -1;
	}
	const size_t buf_size = 0x10000;
	rewind(fp);
	u8 fileBuffer[buf_size];
	u32 startAddr = refDataAddress;
	u32 currentAddr = refDataAddress;
	while(1){
		int bytes = fread(fileBuffer, sizeof(u8), buf_size/sizeof(u8), fp);
		if(!bytes){
			return -1;
		}
		for(int i = 0;i<bytes;i++){
			u8 cartByte = *((u8*)(currentAddr + i));
			u8 cartByte2 = *((u8*)(currentAddr + i + 1));
			if(cartByte != fileBuffer[i]){
				printTop("Diff: Cart:%02X %02X File:%02X %02X\n", cartByte, cartByte2, fileBuffer[i], fileBuffer[i + 1]);
				return currentAddr - startAddr;
			}
		}
		currentAddr += bytes;
	}
}

void verify(){
	std::string path = ".";
	std::string f = getFile("Choose ROM to compare to", path);
	FILE *fp = fopen(f.c_str(), "rb");
	if(!fp){
		printTop("\nUnable to open file.\n");
		return;
	}
	printTop("Comparing...\n");
	int result = verifyFile(fp);
	fclose(fp);
	if(result < 0){
		printTop("Cart matches file\n\n");
	}else{
		printTop("Difference found at 0x%07X\n\n", (unsigned int)result);
		
	}
	
}

int writeSRAM(FILE* fp, u32 saveAddr) {
	if(!fp){
		printTop("No file to verify\n");
		return 0;
	}
	const size_t buf_size = 0x10000;
	rewind(fp);
	u8 fileBuffer[buf_size];
	u32 startAddr = saveAddr;
	u32 currentAddr = saveAddr;
	while(1){
		int bytes = fread(fileBuffer, sizeof(u8), buf_size/sizeof(u8), fp);
		if(!bytes){
			return currentAddr - startAddr;
		}
		for(int i = 0;i<bytes;i++){
			*((u8*)(currentAddr + i)) = fileBuffer[i];
		}
		currentAddr += bytes;
	}
}



int checkBlock(FILE *fp, u16 sector){
	u16 buffer;
	fseek(fp, 0x10000*sector, SEEK_SET);
	fread(&buffer, sizeof(u16), 1, fp);
	if(buffer != readFlash(0x10000*sector)){
		printTop("Sector %d: %02X\nExpected: %02X\n", sector, buffer, readFlash(0x10000*sector));
	}
	return buffer == readFlash(0x10000*sector);
}


int fastVerify(FILE *fp){
	if(fp){
		int sectors = 1+(getFileSize(fp)-1)/0x10000;
		for(int i = 0;i<sectors;i++){
			writeFlash(i*0x10000, 0xFF);
			if(!checkBlock(fp, i)){
				return i;
			}
		}
	}
	return -1;
	
}


static void switchSramBank(int bank){
	writeFlash(0X1000000, bank);
}


u32 memViewTools(u32 address){
	
	Menu m("Tools:");
	m.addOption("Goto Sector");
	m.addOption("Goto SRAM");
	m.addOption("[EG0xx] Goto offset");
	m.addOption("Reset  [x]=F0,[x]=F0F0,[x]=FF");
	m.addOption("Detect [AAA]=90");
	m.addOption("Detect [154]=9898");
	m.addOption("Detect  [0]=90");
	m.addOption("[Intel] Unlock sector");
	m.addOption("[Intel] Erase sector");
	m.addOption("[Intel] Write Word");
	m.addOption("[Intel] Clear Status");
	m.addOption("[MSP128] Erase sector");
	m.addOption("[MSP128] Write Word");
	m.addOption("[MSP100] Erase sector");
	m.addOption("[MSP100] Write Word");
	m.addOption("Goto SRAM Bank 0");	
	m.addOption("Goto SRAM Bank 1");	
	
	int i = m.getDecision();
	switch(i){
		case 0:
		m = numericalMenu("Select sector:", 0, 511, 1);
		i = m.getNumerical();
		if(i == -1){
			return -1;
		}
		return 0x10000*i+0x8000000;
		break;
		
		case 1:
		return 0xA000000;
		break;
		
		case 2:
		i = askMulticartOffset();
		if(i != -1){
			EG0xxGotoChipOffset(i);
		}
		break;
		
		case 3:
		writeFlash(address,0xF0);
		writeFlash(address,0xF0F0);
		writeFlash(address,0xFF);
		break;
		
		case 4:
		writeFlash(0xAAA, 0xA9);
		writeFlash(0x554, 0x56);
		writeFlash(0xAAA, 0x90);
		break;
		
		case 5:
		writeFlash(0x154, 0xF0F0);
		writeFlash(0x154, 0x9898);
		break;
		
		case 6:
		writeFlash(0,0xFF);
		writeFlash(0,0x90);
		break;
		
		case 7:
		writeFlash(address, 0x60);
		writeFlash(address, 0xD0);
		break;
		
		case 8:
		writeFlash(address, 0x20);
		writeFlash(address, 0xD0);
		break;
		
		case 9:
		writeFlash(address, 0x40);
		writeFlash(address, 0x0000);
		break;
		
		case 10:
		writeFlash(address, 0x50);
		break;
		
		case 11:
		writeFlash(0xAAA, 0xA9);
		writeFlash(0x554, 0x56);
		writeFlash(0xAAA, 0x80);
		writeFlash(0xAAA, 0xA9);
		writeFlash(0x554, 0x56);
		writeFlash(address, 0x30);
		break;
		
		case 12:
		writeFlash(0xAAA, 0xA9);
		writeFlash(0x554, 0x56);
		writeFlash(0xAAA, 0x20);
		writeFlash(address, 0xA0);
		writeFlash(address, 0x0000);
		break;
		
		case 13:
		writeFlash(0x1554, 0xAAA9);
		writeFlash(0xAAA, 0x5556);
		writeFlash(0x1554, 0x8080);
		writeFlash(0x1554, 0xAAA9);
		writeFlash(0xAAA, 0x5556);
		writeFlash(address, 0x3030);
		break;
		
		case 14:
		writeFlash(0x1554, 0xAAA9);
		writeFlash(0xAAA, 0x5556);
		writeFlash(0x1554, 0x2020);
		writeFlash(address, 0xA0A0);
		writeFlash(address, 0x0000);
		break;
		
		case 15:
		switchSramBank(0);
		return 0xA000000;

		case 16:
		switchSramBank(1);
		return 0xA000000;

		default:
		break;
	}
	
	
	consoleClear();
	return -1;
}


void memView(){
	consoleClear();
	u32 address = 0x8000000;
	u8 out;
	iprintf("\x1b[19;0H Up/Down - Navigate");
	iprintf("\x1b[20;0H Select - Tools");
	iprintf("\x1b[21;0H A - Fast scroll");
	iprintf("\x1b[22;0H B - Back");
	volatile u16 keys;
	do{
		swiWaitForVBlank();
		iprintf("\x1b[0;0HAddress: 0x%08x\n\n",(unsigned int)address);
		for(int i=0;i<256;i++){
			out = *(u8*)(address+i);
			iprintf("%02x",out);
		}
		
		int scrollSpeed = 1;
		scanKeys();
		keys = keysHeld();
		if(keys & KEY_SELECT){
			int x = memViewTools(address);
			iprintf("\x1b[19;0H Up/Down - Navigate");
			iprintf("\x1b[20;0H Select - Tools");
			iprintf("\x1b[21;0H A - Fast scroll");
			iprintf("\x1b[22;0H B - Back");
			if(x > 0){
				address = x;
			}
		}
		if(keys & KEY_A){scrollSpeed = 50;}
		if(keys & KEY_DOWN){address+=16*scrollSpeed;}
		if(keys & KEY_UP){address-=16*scrollSpeed;}
		
	}while((keys & KEY_B) == 0);
}







int fileExists(char* name){
	FILE* f = fopen(name, "r");
	if(f){
		fclose(f);
		return 1;
	}
	return 0;
}


int dirExists(const char* path){
	DIR *dp = opendir(path);
	if(dp){
		closedir(dp);
		return 1;
	}
	return 0;
}





void BootGbaARM9(u32 useBottomScreen, cBMP15& border)
{	
	//start arm7 sequence
	fifoSendValue32(FIFO_USER_01, useBottomScreen);
	
	
	videoSetMode(MODE_5_2D | DISPLAY_BG3_ACTIVE);
	videoSetModeSub(MODE_5_2D | DISPLAY_BG3_ACTIVE);
	vramSetPrimaryBanks(VRAM_A_MAIN_BG_0x06000000, VRAM_B_MAIN_BG_0x06020000, VRAM_C_SUB_BG_0x06200000, VRAM_D_LCD);

	REG_BG3CNT = BG_BMP16_256x256 | BG_BMP_BASE(0) | BG_WRAP_OFF;
	REG_BG3PA = 1 << 8;
	REG_BG3PB = 0;
	REG_BG3PC = 0;
	REG_BG3PD = 1 << 8;
	REG_BG3VOFS = 0;
	REG_BG3HOFS = 0;
	
	if(border.valid()){
		memcpy((u16*)BG_BMP_RAM(0), border.buffer(), SCREEN_WIDTH*SCREEN_HEIGHT*2);
		memcpy((u16*)BG_BMP_RAM(8), border.buffer(), SCREEN_WIDTH*SCREEN_HEIGHT*2);
	}else{
		memset((void*)BG_BMP_RAM(0), 0, 0x18000);
		memset((void*)BG_BMP_RAM(8), 0, 0x18000);
	}
	
	//POWCNT = 8003h / 0003h
	if(useBottomScreen) {
        REG_POWERCNT =  3;
    }else {
        REG_POWERCNT = 0x8003;
    }
	
	REG_EXMEMCNT |= 0x8080;     // ARM7 has access to GBA cart
	REG_EXMEMCNT &= ~0x4000;    // set Async Main Memory mode
	
	// halt indefinitly, since irqs are disabled
	REG_IME = 0;
	swiWaitForIRQ();
	
}


bool checkSramBankSwitching(){
	vu8* sram = (vu8*)0xA000010;
	switchSramBank(0);

	vu8 bank0data = *sram;
	(*sram)++;
	if(*sram != (u8)(bank0data+1)){
		return false;	//writing to sram fails
	}
	(*sram)--;

	switchSramBank(1);
	vu8 bank1data = *sram;
	if(bank0data != bank1data){
		return true;	//bank1 has different data than bank0
	}

	(*sram)++;
	switchSramBank(0);
	if(*sram == (u8)(bank1data+1)){
		//bank1 has same data as bank0 and incrementing bank1 affects bank0 => no bank switching
		*sram = bank0data;
		return false;
	}
	
	//incrementing bank1 didn't affect bank0, revert change to bank1
	switchSramBank(1);
	(*sram)--;
	return true;
}

bool isHeaderMirroredAt(u32 addr){
	return memcmp((void*)0x8000000, (void*)(addr|0x8000000), 0xC0) == 0;
}


