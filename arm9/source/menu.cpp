#include "menu.h"

Menu::Menu(const char* title){
	clear();
	setTitle(title);
}

Menu::Menu(){
	clear();
}

void Menu::clear(){
	title = "";
	height = CONSOLE_HEIGHT;
	selected = 0;
	offset = 0;
	options.clear();	
}

void Menu::addOption(std::string name){
	options.push_back(name);
}

int Menu::getDecision(){
	if(options.size() < 1){return -1;}
	bool hasChosen = false;
	do{
	
		scanKeys();

		if(keysDown() & KEY_UP){
			moveUp();
		}else if(keysDown() & KEY_DOWN){
			moveDown();
		}
		
		if(keysDown() & KEY_A){
			hasChosen = 1;
		}
		if(keysDown() & KEY_B){
			hasChosen = 1; selected = -1;
		}

		if(keysHeld() & KEY_DOWN && timer > -20){
			timer--;
		}else if(keysHeld() & KEY_UP && timer < 20){
			timer++;
		}else timer = 0;

		if(timer == 20 || timer == -20){
			while(keysHeld() & KEY_UP){
				moveUp();
				swiWaitForVBlank();
				printSelection();
				scanKeys();
			}
			while(keysHeld() & KEY_DOWN){
				moveDown();
				swiWaitForVBlank();
				printSelection();
				scanKeys();
			}
		}

		swiWaitForVBlank();
		printSelection();

	}while(hasChosen == false);
return selected;
}


void Menu::printSelection(){
	consoleClear();
	printf("%s\n\n", title);
	//offset:int selected:int 
	for(int i=0;i<CONSOLE_HEIGHT-2;i++){
		if((int)selected == (int)(i+offset)){
			iprintf(">");
		}
		if((int)(i+offset)<(int)options.size()){
			printf(" %s\n", options.at(i+offset).c_str());
		}
	}
}


void Menu::setTitle(const char* t){
	title = t;
}

int Menu::size(){
	return options.size();
}



void Menu::moveUp(){
	if(selected == 0){
		return;
	}
	if(selected-offset == 0){
		selected--;
		offset--;
	}else{
		selected--;
	}
}

void Menu::moveDown(){
	if(selected >= (int)options.size()-1){
		return;
	}
	if(selected-offset == height-3){
		selected++;
		offset++;
	}else{
		selected++;
	}

	
}



int Menu::getNumerical(){
	
	selected = numerical_min;
	bool hasChosen = false;
	
	do{
	
		scanKeys();

		if(keysDown() & KEY_UP && selected < numerical_max){
			selected++;

		}else if(keysDown() & KEY_DOWN && selected > numerical_min){
			selected--;
		}
		
		if(keysDown() & KEY_A){
			hasChosen = 1;
		}
		if(keysDown() & KEY_B){
			consoleClear();
			return -1;
		}

		if(keysHeld() & KEY_DOWN && timer > -20){
			timer--;
		}else if(keysHeld() & KEY_UP && timer < 20){
			timer++;
		}else timer = 0;

		if(timer == 20 || timer == -20){
			while(keysHeld() & KEY_UP && selected < numerical_max){
				selected++;
				swiWaitForVBlank();
				printNumericalSelection();
				scanKeys();
			}
			while(keysHeld() & KEY_DOWN && selected > numerical_min){
				selected--;
				swiWaitForVBlank();
				printNumericalSelection();
				scanKeys();
			}
		}

		swiWaitForVBlank();
		printNumericalSelection();

	}while(hasChosen == false);
	consoleClear();
	return selected*numerical_increment;
}



void Menu::printNumericalSelection(){
	consoleClear();
	printf("%s %02X\n\nUp/Down - Select\nA - Confirm\nB - Back", title, selected*numerical_increment);	
}




Menu numericalMenu(const char* title, int min, int max, int increment){
	Menu menu;
	menu.setTitle(title);
	menu.numerical_min = min/increment;
	menu.numerical_max = max/increment;
	menu.numerical_increment = increment;
	return menu;
}
