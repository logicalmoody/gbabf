#include <fat.h>
#include "utils.h"


#define ROM_DIR "GBA"
#define DUMP_DIR "GBA_BACKUP"
#define FRAME_DIR "frames"


void flashRom();
void dumpData();
void injectData();

void dumpSRAM();
void injectSRAM();
void eraseSRAM();
void injectData();

void launchSlot2();
void eraseFlash();



int main(){
	initGraphics();
	fatInitDefault();
	printTop("GBABF 1.2 - GBA Bootleg Flasher\n\n");
	
	//slow down gba slot access time for possible stability
	REG_EXMEMCNT = 0xF;
		
	while(1){
		Menu *modeSelect = new Menu;
		modeSelect->setTitle("Select A Mode");
		
		modeSelect->addOption("Flash ROM");
		modeSelect->addOption("Detect Flash");
		modeSelect->addOption("Compare cart to file");
		modeSelect->addOption("ROM Viewer");
		modeSelect->addOption("List EG0xx-Multicart Games");
		modeSelect->addOption("Dump data");
		modeSelect->addOption("Flash data");
		modeSelect->addOption("Erase flash");
		modeSelect->addOption("Backup SRAM");
		modeSelect->addOption("Restore SRAM");
		modeSelect->addOption("Erase SRAM");
		modeSelect->addOption("Launch Slot-2 Game");
		
		int mode = modeSelect->getDecision();

		switch(mode){
			case 0:
				flashRom();
			break;
	
			case 1:
				detectionTest();
			break;
	
			case 2:
				verify();
			break;
			
			case 3:
				memView();
			break;
			
			case 4:
				listMulticartGames();
			break;
			
			case 5:
				dumpData();
			break;
			
			case 6:
				injectData();
			break;
			
			case 7:
				eraseFlash();
			break;

			case 8:
				dumpSRAM();
			break;

			case 9:
				injectSRAM();
			break;

			case 10:
				eraseSRAM();
			break;

			case 11:
				launchSlot2();
			break;
		}

	delete modeSelect;
	}
	
}






void launchSlot2(){
	if(!isGame()){ //Nintendo logo is incorrect and the game won't boot
		printTop("WARNING: ROM header is invalid!\n");
	}
	
	u32 screen = PersonalData->gbaScreen;
	cBMP15 border;
	const std::string screenStrings[] = {"Screen: Top", "Screen: Bottom"};
	std::string dir = "/" FRAME_DIR;
	
	while(1){
		Menu gbaOptions("Launch Slot-2 Game");
		gbaOptions.addOption("Launch");
		gbaOptions.addOption(screenStrings[screen]);
		gbaOptions.addOption("Set Custom Border...");
		switch(gbaOptions.getDecision()){
			case 0:
				BootGbaARM9(screen, border);
			break;
			
			case 1:
				screen = (screen+1)&1;
			break;
			
			case 2:
				border = createBMP15FromFile(getFile("Select border (15bit BMP)", dir).c_str());
				if(!border.valid()){
					printTop("Selected border was invalid!\n");
				}else{
					printTop("Border set!\n");
				}
			break;
			
			
			default:
			return;
		}
	}
	
}

void dumpDataToFile(u32 address, const int bytes, FILE* fp) {
	// Attempt to use 64kB block sizes, fall back to 32kB if 64kB is too large.
	int BLOCKSIZE = 64*1024;
	int blocks = bytes / BLOCKSIZE;
	if (bytes % BLOCKSIZE != 0){
		BLOCKSIZE = 32*1024;
		blocks = bytes / BLOCKSIZE;
		if (bytes % BLOCKSIZE != 0){
			printTop("ERROR: Data must be divisible by 64k\n");
			return;
		}
	}
	for(int i = 0;i<blocks;i++){
		printf("\x1b[2;6H=== Dumping data ===\x1b[10;4HSaving blocks(%d / %d)", i, blocks);
		u8 arr[BLOCKSIZE];
		for (int j = 0; j < BLOCKSIZE; j++){
			arr[j] = *((u8*)(address + j));
		}
		// printTop("Arr: [%02x, %02x, %02x, %02x]\n", arr[0], arr[1], arr[2], arr[3]);
		fwrite(arr, 1, BLOCKSIZE, fp);
		address += BLOCKSIZE;
	}
}

void chooseFileName(char* nameBuffer, char const* extension) {
	if(dirExists("/" DUMP_DIR)){
		strncpy(nameBuffer, DUMP_DIR "/", strlen(DUMP_DIR)+1);
	}

	if(*(char*)0x80000A0){
		strncpy(&nameBuffer[strlen(nameBuffer)], (char*)0x80000A0, 12);
	}else{
		strcpy(&nameBuffer[strlen(nameBuffer)], "Untitled");
	}

	const int lenNoExt = strlen(nameBuffer);
	sprintf(&nameBuffer[lenNoExt], ".%s", extension);

	u16 nameIndex = 0;
	while(fileExists(nameBuffer) && nameIndex < 10000){
		nameIndex++;
		sprintf(&nameBuffer[lenNoExt], "(%d).%s",nameIndex, extension);
	}
}

void dumpData(){
	u32 dumpSrcAddr = numericalMenu("Enter dump start offset\n", 0, 0x1FF0000, 0x10000).getNumerical();
	if(dumpSrcAddr < 0){
		printTop("Dump cancelled.\n");
		return;
	}
	
	Menu sizeMenu("Enter dump size:");
	sizeMenu.addOption("64 kbytes");
	sizeMenu.addOption("128 kbytes");
	sizeMenu.addOption("4 Mbytes");
	sizeMenu.addOption("8 Mbytes");
	sizeMenu.addOption("16 Mbytes");
	sizeMenu.addOption("32 Mbytes");
	int bytes;
	
	switch(sizeMenu.getDecision()){
		case 0:
			bytes = 64*1024;
		break;
		
		case 1:
			bytes = 128*1024;
		break;
		
		case 2:
			bytes = 4*1024*1024;
		break;
		
		case 3:
			bytes = 8*1024*1024;
		break;
		
		case 4:
			bytes = 16*1024*1024;
		break;
		
		case 5:
			bytes = 32*1024*1024;
		break;
		
		default:
		printTop("Dump cancelled.\n");
		return;
	}

	
	char name[64] = {};
	chooseFileName(name, "bin");
	
	FILE *fp = fopen(name, "wb");
	if(!fp){
		printTop("Unable to open file\n");
		return;
	}
	
	printTop("Saving as:\n");
	printTop("%s\n", name);

	consoleClear();
	printf("\x1b[7;4H%s", name);

	dumpDataToFile(0x8000000+dumpSrcAddr, bytes, fp);
	fclose(fp);
	printTop("Dump completed!\n\n");

}






void injectData(){
	std::string dir = dirExists("/" DUMP_DIR) ? "/" DUMP_DIR : ".";
	std::string f = getFile("Choose data file to flash", dir);
	
	FILE *fp = fopen(f.c_str(), "rb");
	if(!fp){
		printTop("\nUnable to open file.\n");
		return;
	}
	
	int saveAddr = numericalMenu("Enter data destination address\n", 0, 0x1FF0000, 0x10000).getNumerical();
	if(saveAddr == -1){
		printTop("Data insertion cancelled\n");
		return;
	}
	
	startFlashingUI();
	if(writeFlashRom(fp, detect(), saveAddr)){
		printTop("Flashing failed.\n");
	}
	stopFlashingUI();
	fclose(fp);
}

void dumpSRAM(){
	u32 sramAddress = 0xa000000;

	Menu sizeMenu("Enter dump size:");
	sizeMenu.addOption("32 kbytes");
	sizeMenu.addOption("64 kbytes");

	int bytesChoises[] = {
		32*1024,
		64*1024,
	};

	int bytes = bytesChoises[sizeMenu.getDecision()];

	char name[64] = {};
	chooseFileName(name, "sav");

	{   // Dump SRAM
		FILE *fp = fopen(name, "wb");
		if(!fp){
			printTop("Unable to open file\n");
			return;
		}

		printTop("Saving SRAM as:\n");
		printTop("%s\n", name);

		consoleClear();
		printf("\x1b[7;4H%s", name);

		dumpDataToFile(sramAddress, bytes, fp);
		fclose(fp);
	}

	{   // Verify SRAM
		FILE *fp = fopen(name, "rb");
		if(!fp){
			printTop("\nUnable to open file.\n");
			return;
		}
		printTop("Verifying dump,");
		int result = verifyFile(fp, sramAddress);
		fclose(fp);
		if(result < 0){
			printTop(" OK!\n");
			printTop("Dump completed!\n\n");
		}else{
			printTop(" FAIL!\n");
			printTop("Difference found at 0x%07X\n", (unsigned int)result);
			printTop("Dump completed with errors!\n\n");
		}
	}
}


void injectSRAM(){
	std::string dir = dirExists("/" DUMP_DIR) ? "/" DUMP_DIR : ".";
	std::string f = getFile("Choose SRAM file to flash", dir);
	int saveAddr = 0xa000000;

	{   // Write SRAM
		FILE *fp = fopen(f.c_str(), "rb");
		if(!fp){
			printTop("\nUnable to open file.\n");
			return;
		}

		printTop("Flashing SRAM\n");
		if((unsigned int)(writeSRAM(fp, saveAddr)) < f.size()){
			printTop("File ended early.\n");
		}
		fclose(fp);
	}
	{   // Verify SRAM
		FILE *fp = fopen(f.c_str(), "rb");
		if(!fp){
			printTop("\nUnable to open file.\n");
			return;
		}
		printTop("Verifying SRAM,");
		int result = verifyFile(fp, saveAddr);
		fclose(fp);
		if(result < 0){
			printTop(" OK!\n");
			printTop("Flash completed!\n\n");
		}else{
			printTop(" FAIL!\n");
			printTop("Difference found at 0x%07X\n", (unsigned int)result);
			printTop("Flash completed with errors!\n\n");
		}
	}
}


void eraseSRAM() {
	u32 sramAddress = 0xa000000;

	Menu sizeMenu("Enter SRAM size to erase:");
	sizeMenu.addOption("32 kbytes");
	sizeMenu.addOption("64 kbytes");

	int bytesChoises[] = {
		32*1024,
		64*1024,
	};

	int bytes = bytesChoises[sizeMenu.getDecision()];
	printTop("Erasing SRAM\n");
	// Has to be volitile to ensure that the compiler do not optimize it away.
	volatile u8 FF = 0xFF;
	for(int i = 0;i<bytes;i++){
		*((u8*)(sramAddress + i)) = FF;
	}
	printTop("Verifying SRAM,");
	for(int i = 0;i<bytes;i++){
		int ok = *((u8*)(sramAddress + i)) == FF;
		if (!ok) {
			printTop(" FAIL!\n");
			printTop("Difference found at 0x%07X\n", (unsigned int)i);
			printTop("Erase completed with errors!\n\n");
			return;
		}
	}
	printTop(" OK!\n");
	printTop("Erase completed!\n\n");
}




void flashRom(){
	std::string dir = dirExists(ROM_DIR) ? "./" ROM_DIR : ".";
	printTop("\n\n");
	std::string f = getFile("Choose ROM to flash", dir);
	FILE *fp = fopen(f.c_str(), "rb");
	if(!fp){
		printTop("\nUnable to open file.\n");
		return;
	}
	Menu *proto = new Menu("Choose Flashing Method");
	proto->addOption("Auto-Detect");
	proto->addOption("MSP128");
	proto->addOption("MSP100");
	proto->addOption("Intel");
	proto->addOption("Intel Buffered");
	proto->addOption("MSP128 (data flip)");
	proto->addOption("MSP100 (address shift)");
	proto->addOption("Intel Buffered (data flip)");
	proto->addOption("EG0xx Multicart");
	
	cartType methods[] = {{MSP128, 0}, {MSP100, 0}, {INTEL, 0}, {INTEL_BUFFERED, 0}, {MSP128,1}, {MSP100SHIFTED,0}, {INTEL_BUFFERED,1}};
	int p = proto->getDecision();
	delete proto;
	u32 addr;
	int result;
	
	if(p < 0){
		fclose(fp);
		return;
	}
	
	switch(p){
		case 0:
			startFlashingUI();
			result = writeFlashRom(fp, detect());
		break;
		
		case 8:
		addr = askMulticartOffset();
			//B was pressed
			if(addr < 0){
				fclose(fp);
				return;
			}
			startFlashingUI();
			result = writeEG0xxMultiCart(fp, addr);
		break;
		
		default:
			startFlashingUI();
			result = writeFlashRom(fp, methods[p-1]);
		break;
		
	}
	if(result < 0){
		printTop("Flashing failed.\n\n");
	}else{
		int failedBlock = fastVerify(fp);
		if(failedBlock >= 0){
			printTop("Possible error on sector %d\n\n", failedBlock);
		}else{
			printTop("Completed in %d seconds\n\n", gStatus.seconds);
		}
	}
	stopFlashingUI();
	fclose(fp);
}



void eraseFlash(){
	Menu proto("Choose Erasing Method");
	proto.addOption("Auto-Detect");
	proto.addOption("MSP128");
	proto.addOption("MSP100");
	proto.addOption("Intel");
	proto.addOption("MSP128 (data flip)");
	proto.addOption("MSP100 (address shift)");
	
	cartType methods[] = {{MSP128, 0}, {MSP100, 0}, {INTEL, 0}, {MSP128,1}, {MSP100SHIFTED,0}};
	int p = proto.getDecision();

	if(p == -1){
		printTop("Erase cancelled!\n");
		return;
	}
	int result;
	startFlashingUI();
	if(!p){
		result = eraseFlashRom(detect());
	}else{
		result = eraseFlashRom(methods[p-1]);
	}
	stopFlashingUI();
	
	if(result != -1){
		printTop("Erase completed!\n");
	}else{
		printTop("Erase aborted!\n");
	}
	
}
