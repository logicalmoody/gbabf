#pragma once

#include <nds.h>
#include <stdio.h>

//For use with waitForFlash()
#define WAIT_STATUS_READY [](u16 a){return (a&0x80)>0;}
#define WAIT_NON_FFFF [](u16 a){return a!=0xFFFF;}
#define WAIT_FOR_FFFF [](u16 a){return a==0xFFFF;}
#define WAIT_NON_0 [](u16 a){return a!=0;}
#define WAIT_FOR_0 [](u16 a){return a==0;}

const u32 TIMEOUT_5S = 0xA00000;
const u32 TIMEOUT_8S = 0xF00000;

enum protocol {UNKNOWN, MSP100, MSP128, INTEL, INTEL_BUFFERED, MSP100SHIFTED};

typedef struct{
	u16 sectorsErased;
	u16 totalSectors;
	u32 wordsFlashed;
	int seconds;
}opStatus;

typedef struct{
	protocol method;
	u8 flags;
}cartType;

typedef struct{
	char* data;
	u32 size;
}rombuffer;

extern void printTop(const char* str, ...);
extern u8 gFlippedDataBits;
extern u32 gSectorSize;
extern char gDatabuf[0x10000];
extern rombuffer gRomBuf;
extern opStatus gStatus;

const u8 CART_TYPE_FLIP_DATA_BITS         = 0x1 << 0;
const u8 CART_TYPE_IGNORE_WAIT_FOR_UNLOCK = 0x1 << 1;

u8 getFlipDataBitsFlag(cartType cartType);
u8 getWaitForUnlockFlag(cartType cartType);

u32 getFileSize(FILE* fp);
vu16 readFlash(u32 addr);
vu8 readFlashByte(u32 addr);
volatile void writeFlash(u32 addr, u16 data);
void writeFlashFlip(u32 addr, u16 data);
void writeArray(u32 addr, u8 len, u16 data[]);
cartType detect();
bool waitForFlash(u32 address, bool (*isReady)(u16), int timeout);

int eraseFlashRom(cartType type);
int writeFlashRom(FILE* fp, cartType type, u32 startAddr = 0);

int eraseSectorMSP100(int sector);
int flashMSP100(FILE* rom, u32 startAddr = 0);

int eraseSectorMSP100Shifted(int sector);
int flashMSP100Shifted(FILE* rom, u32 startAddr = 0);

int unlockSectorIntel(int sector);
int eraseSectorIntel(int sector);
int flashIntelBuffered(FILE* rom, u32 startAddr = 0);
int flashIntel(FILE *rom, u32 startAddr = 0, cartType type = {INTEL, 0});
int writeWordIntel(u32 addr, u16 data);


void resetMSP128();
int eraseSectorMSP128(int sector);
int flashMSP128(FILE *rom, u32 startAddr = 0);

//offset in megabytes
int writeEG0xxMultiCart(FILE *f, u32 offset);
void EG0xxGotoChipOffset(u32 offset);