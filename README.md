# GBABF - GBA Bootleg Flasher

A tool for reflashing bootleg GBA cartridges on the Nintendo DS

![](https://gitlab.com/Fexean/gbabf/-/raw/master/screenshot.png)

**USE AT YOUR OWN RISK!**  
Might render your bootlegs unplayable  
Creating a [backup](https://www.gamebrew.org/wiki/GBA_Backup_Tool) of the cart beforehand is adviced  
I recommend placing your GBA roms in a directory called "GBA" on the root of your SD-card, as it will be opened by default.  


## Building
Have [Devkitpro](https://devkitpro.org/wiki/Getting_Started) installed and run `make`


### [Binary Download](https://gitlab.com/Fexean/gbabf/uploads/3cb7b080a25f43794b125a547367c993/gbabf_1.2.7z)
